# Open Community's Note app

## Useful links

| Link                                                | Description                |
|-----------------------------------------------------|----------------------------|
| [localhost:5000](http://localhost:5000)             | Application through docker |
| [localhost:3000](http://localhost:3000)             | NodeJS server              |
| [localhost:8080](http://localhost:8080)             | Application devserver      |
| [localhost:3000/_/api](http://localhost:3000/_/api) | OpenApi reader             |
| [localhost:3000/_/ui](http://localhost:3000/_/ui)   | Swagger stats page         |
| [localhost:9000/](http://localhost:9000)            | PgAdmin 4                  |

## Useful credentials 

| Login                       | Password   | Context  | Description      |
|-----------------------------|------------|----------|------------------|
| `postgres`                  | `password` | Postgres | Root password    |
| `popchef`                   | `password` | Postgres | Application user | 
| `admin@popchef-recruitment` | `password` | PgAdmin4 | Main user        |

## Installation

### Pre-requisites

* nodeJS 12+
* docker 19+
* docker-compose 1.24+
* git 2.11+

Previous version may work too.

### Fetch repository and submodules
```bash
git clone git@gitlab.com:AbrahamTewa/popchef-recruitment.git
cd popchef-recruitment
git submodule init
git submodule update
```

### Development install

This step are mandatory to setup development environment

```bash
cd back
npm ci

cd ../front
npm ci
```

## Usage

### Docker only

```
docker-compose up -d
```

This will:
* Build the application locally
* start the application
* start the postgres database
* start the pgadmin4 instance
* the popchef node server with the frontend application

At this point, the `popchef.app` container failed: since database was not yet mounted, it didn't found it and stopped.
You will have to restart it manually

```
docker-compose up -d
```

Last: you will have to create a user for your application:

```
docker-compose run app user create admin@popchef-recruitment password
```

(or use any other login/password)

You can access then the application by navigating too http://localhost:5000

### Development

Start the database and pgadmin:
```
docker-compose up db db.admin -d
```

Run backend server in development mode:

```
cd back
npm run dev
```

This will start a server, accessible through http://localhost:3000.

Create the application user (if necessary):

```
npm run cli:src -- user create admin@popchef-recruitment password
```

(or use any other login/password)

Start the client devserver:

```
cd front
npm run dev
```

This will start the client, accessible through http://localhost:8080.
