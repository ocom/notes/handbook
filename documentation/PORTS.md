# Ports

| Ports | Service              | Description         |
| 3000  | `notes.lb`           | Main nginx server   |
| 3001  | `notes.api`          | Main backend server |
| 5432  | `notes.db`           | PostgresSQL server  |
| 9000  | `notes.db.admin`     | Pgadmin application |
| 9001  | `notes.api.editor`   | Swagger Editor      |
