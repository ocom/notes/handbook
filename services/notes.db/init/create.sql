CREATE ROLE notes WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION
  ENCRYPTED PASSWORD 'md595670acb263d25d80fd8cc2f77558105'; -- Password: "password"

CREATE SCHEMA app
    AUTHORIZATION notes;

GRANT ALL PRIVILEGES ON SCHEMA app TO notes;

SET SESSION AUTHORIZATION notes;
SET search_path TO app;

-- ==============================
-- Table : USERS

CREATE TABLE USERS (
    id                  SERIAL          NOT NULL,
    email               VARCHAR(256)    NOT NULL,
    password_hash       VARCHAR(256)    NOT NULL,
    password_salt       VARCHAR(512)    NOT NULL,
    first_name          VARCHAR(256),
    last_name           VARCHAR(256),
    created_at          TIMESTAMPTZ     NOT NULL,
    updated_at          TIMESTAMPTZ     NOT NULL,
    deleted_at          TIMESTAMPTZ,

    CONSTRAINT pk_USERS       PRIMARY KEY (id),
    CONSTRAINT uq_USERS_email UNIQUE (email)
);

-- ==============================
-- Table : NOTES

CREATE TABLE NOTES (
    id              SERIAL         NOT NULL,
    title           VARCHAR(128)   NOT NULL,
    content         VARCHAR(1024)  NOT NULL,
    owner_id        INTEGER        NOT NULL,
    archived        BOOLEAN        NOT NULL DEFAULT FALSE,
    created_at      TIMESTAMPTZ    NOT NULL,
    updated_at      TIMESTAMPTZ    NOT NULL,
    deleted_at      TIMESTAMPTZ,

    CONSTRAINT pk_NOTES PRIMARY KEY (id),

    CONSTRAINT fk_NOTES_owner
        FOREIGN KEY (owner_id)
        REFERENCES USERS(id)
);

-- ==============================
-- Table : NOTE_SHARES

CREATE TABLE NOTE_SHARES (
    user_id         INTEGER     NOT NULL,
    note_id         INTEGER     NOT NULL,
    allow_share     BOOLEAN     NOT NULL DEFAULT FALSE,
    allow_update    BOOLEAN     NOT NULL DEFAULT FALSE,
    shared_by       INTEGER     NOT NULL,
    share_at        TIMESTAMPTZ NOT NULL,

    CONSTRAINT pk_NOTE_SHARES PRIMARY KEY (user_id, note_id),

    CONSTRAINT fk_NOTE_SHARES_user
        FOREIGN KEY (user_id)
        REFERENCES USERS(id),

    CONSTRAINT fk_NOTE_SHARES_note
        FOREIGN KEY (note_id)
        REFERENCES NOTES(id)
);

COMMENT ON COLUMN NOTE_SHARES.user_id IS 'User with which the note is shared';
COMMENT ON COLUMN NOTE_SHARES.note_id IS 'Note shared';
COMMENT ON COLUMN NOTE_SHARES.allow_update IS 'If true, then the user is allowed to update the note';
COMMENT ON COLUMN NOTE_SHARES.allow_share IS 'If true, then the user is allowed to manage sharing of the note';

-- ==============================
-- Table : TAGS

CREATE TABLE TAGS (
    id              SERIAL          NOT NULL,
    label           VARCHAR(128)    NOT NULL,
    owner_id        INTEGER         NOT NULL,
    created_at      TIMESTAMPTZ     NOT NULL,
    updated_at      TIMESTAMPTZ     NOT NULL,
    deleted_at      TIMESTAMPTZ,

    CONSTRAINT pk_TAGS PRIMARY KEY (id),

    CONSTRAINT fk_TAGS_owner
        FOREIGN KEY (owner_id)
        REFERENCES USERS(id)
);

-- ==============================
-- Table : NOTE_TAGS

CREATE TABLE NOTE_TAGS (
    note_id         INTEGER         NOT NULL,
    tag_id          INTEGER         NOT NULL,
    created_at      TIMESTAMPTZ     NOT NULL,
    updated_at      TIMESTAMPTZ     NOT NULL,
    deleted_at      TIMESTAMPTZ,

    CONSTRAINT pk_NOTE_TAGS PRIMARY KEY (note_id, tag_id),

    CONSTRAINT fk_NOTE_TAGS_note 
        FOREIGN KEY (note_id)
        REFERENCES NOTES(id),

    CONSTRAINT fk_NOTE_TAGS_tag
        FOREIGN KEY (tag_id)
        REFERENCES TAGS(id)
);
